package com.hiro.movak.model.services;

import com.hiro.movak.model.database.Database;
import com.hiro.movak.model.entities.*;
import com.hiro.movak.model.utils.Token;
import com.sun.istack.NotNull;
import org.hibernate.Transaction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * @author hiro
 */

public class TextService
{
    /* Private member variables ***************************************************************************************/

    private static final String nonAlphabetic = "[0-9„“«»\\[\\]():;,?!.]";

    private Database db;

    private WordService wordService = new WordService();

    private List<Construction> constructionList = null;

    /* Public methods *************************************************************************************************/

    /**
     *
     */

    public TextService()
    {
        db = Database.getInstance();
    }

    /**
     *
     * @param text
     * @param textFile
     */

    public void createText(Text text, File textFile)
    {
        // Read text content
        String contentString = readTextFile(textFile);

        // Split text by spaces
        List<String> wordList = convertToList(contentString);

        // Add new word forms
        addForms(wordList);

        // Add text and text content
        addContent(wordList, text);
    }

    /**
     *
     * @return
     */

    public List<Text> getAllTexts()
    {
        db.openSession();

        List<Text> textList = db.textDao.findAll();

        db.closeSession();

        return textList;
    }

    /**
     *
     * @param idList
     * @param location
     * @param length
     * @return
     */

    public List<Token> getTextContent(List<Integer> idList, int location, int length)
    {
        db.openSession();

        // Add appropriate numbers of entries to the list
        List<Token> wordList = new ArrayList<>();
        for (int i = location; i < location + length; i++)
        {
            TextContent content = db.textContentDao.find(idList.get(i));

            String word = content.getForm().getForm();
            Pair pair = content.getPair();

            UserPairDetails pairDetails = null;
            if (pair != null)
                pairDetails = db.userPairDetailsDao.findByPairId(pair.getPairId());

            Token token;
            if (word.equals(" "))
                token = new Token(word, Token.Type.SPACE, i);
            else if (word.matches(nonAlphabetic))
                token = new Token(word, Token.Type.PUNCTUATION, i);
            else if (pairDetails != null)
                token = new Token(word, Token.Type.LEARNING, pairDetails.getFamiliarity(), i);
            else
                token = new Token(word, Token.Type.UNKNOWN, i);

            wordList.add(token);
        }

        db.closeSession();

        return wordList;
    }

    /**
     *
     *
     * @param title
     * @return
     */

    public int getTextId(String title)
    {
        db.openSession();
        List<Text> texts = db.textDao.findByTitle(title);

        if (!texts.isEmpty())
            return texts.get(0).getTextId();
        else
            return -1;
    }

    /**
     *
     * @param textId
     * @return
     */

    public List<Integer> getIdList(int textId)
    {
        db.openSession();

        byte[] contentBytes = db.textDao.find(textId).getContent();
        List<Integer> idList = bytesToIdList(contentBytes);
        /*List<TextContent> textContents = db.textContentDao.findByTextId(textId);
        Collections.sort(textContents, new TextContentComparator());

        List<Integer> idList = new ArrayList<>();
        for (TextContent word : textContents)
            idList.add(word.getTextContentId());*/

        db.closeSession();

        return idList;
    }

    /**
     *
     * @param textId
     * @return
     */

    public UserTextDetails getUserTextDetails(int textId)
    {
        db.openSession();

        UserTextDetails userTextDetails = db.userTextDetailsDao.findByTextId(textId);

        db.closeSession();

        return userTextDetails;
    }

    /**
     *
     * @param userTextDetails
     */

    public void updateUserTextDetails(UserTextDetails userTextDetails)
    {
        db.openSession();
        Transaction transaction = db.getTransaction();

        db.userTextDetailsDao.update(userTextDetails);

        transaction.commit();
        db.closeSession();
    }


    public void updateTextContent(int textId, int pairId)
    {
        db.openSession();
        Transaction transaction = db.getTransaction();

        Pair pair = db.pairDao.find(pairId);

        List<TextContent> contentList = db.textContentDao.findByTextId(textId);
        List<String> inflexions = wordService.getInflexions(pair.getLemma().getLemma());

        for (TextContent content : contentList)
        {
            // Test all possible word forms
            for (String inflexion : inflexions)
            {
                if (content.getForm().getForm().equals(inflexion))
                {
                    content.setPair(pair);
                    db.textContentDao.update(content);
                }
            }
        }

        transaction.commit();
        db.closeSession();
    }

    /* Private methods ************************************************************************************************/

    /**
     *
     * @param textFile
     * @return
     */

    @NotNull
    private String readTextFile(File textFile)
    {
        StringBuilder contentString = new StringBuilder();
        try
        {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(textFile));

            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                contentString.append(line);
            }
        } catch (IOException e)
        {
            System.out.println(e.getMessage());
        }

        return contentString.toString();
    }

    /**
     *
     * @param textString
     * @return
     */

    @NotNull
    private List<String> convertToList(String textString)
    {
        List<String> formattedText = new ArrayList<>();

        String word = "";
        boolean end = false;
        for (int i = 0; i < textString.length(); i++)
        {
            String ch = Character.toString(textString.charAt(i));

            if (ch.matches("\\s"))
                end = true;
            else if (ch.matches(nonAlphabetic))
                end = true;
            else
                word += ch;

            if (end)
            {
                if (!word.equals(""))
                    formattedText.add(word);
                formattedText.add(ch);
                word = "";
                end = false;
            }
        }

        return formattedText;
    }

    /**
     *
     * @param wordList
     */

    private void addForms(List<String> wordList)
    {
        db.openSession();
        Transaction transaction = db.getTransaction();

        // Make a list of strings of words that are already in the database
        List<Form> allForms = db.formDao.findAll();
        List<String> allStrings = new ArrayList<>();
        for (Form form : allForms)
            allStrings.add(form.getForm());

        // Add new words to the database
        Set<String> wordSet = new LinkedHashSet<>(wordList);
        List<String> uniqueWordList = new ArrayList<>(wordSet);
        for (String word : uniqueWordList)
        {
            if (!allStrings.contains(word))
                db.formDao.save(new Form(word, null, null));
        }

        transaction.commit();
        db.closeSession();
        System.out.println("committed");
    }

    /**
     *
     * @param wordList
     * @param text
     */

    private void addContent(List<String> wordList, Text text)
    {
        db.openSession();
        Transaction transaction = db.getTransaction();

        text.setLength(wordList.size());
        db.textDao.save(text);

        List<Form> allForms = db.formDao.findAll();
        List<Integer> idList = new ArrayList<>();

        for (int i = 0; i < wordList.size(); i++)
        {
            Form form = null;
            for (Form f : allForms)
            {
                if (f.getForm().equals(wordList.get(i)))
                {
                    form = f;
                    break;
                }
            }

            if (form == null)
                form = getForm(wordList.get(i));    // question: why is form sometimes null? why is this line needed

            //Pair pair = getPair(wordList.get(i));
            Pair pair = null;

            TextContent textContent = new TextContent(text, i, form, pair);
            int id = db.textContentDao.save(textContent);
            idList.add(id);
        }

        byte[] content = idListToBytes(idList);
        text.setContent(content);
        db.textDao.update(text);

        User user = db.userDao.findByName("default").get(0);
        UserTextDetails userTextDetails = new UserTextDetails(user, text, 0, 0.0f, 0);  // fixme: set knownWords properly
        db.userTextDetailsDao.save(userTextDetails);

        transaction.commit();
        db.closeSession();
        System.out.println("committed");
    }

    /**
     *
     * @param word
     * @return
     */

    private Form getForm(String word)
    {
        Form form = db.formDao.findByForm(word);
        if (form == null)
        {
            form = new Form(word, null, null);
            db.formDao.save(form);
        }

        return form;
    }

    /**
     *
     * @param form
     * @return
     */

    private Pair getPair(String form)
    {
        Lemma lemma = getLemma(form);

        if (lemma == null)
            return null;

        // Find the first matching pair
        List<Pair> pairCandidates = db.pairDao.findByLemma(lemma);
        if (pairCandidates.size() == 0)
            return null;
        else
            return pairCandidates.get(0);
    }

    /**
     *
     * @param form
     * @return
     */

    private Lemma getLemma(String form)
    {
        if (constructionList == null)
            constructionList = db.constructionDao.findAll();

        List<String> candidates = new ArrayList<>();
        for (Construction construction : constructionList)
        {
            if (form.matches(construction.getFormRegex()))
                candidates.add(wordService.getLemmaString(form, construction.getLemmaRegex(), construction.getFormRegex()));
        }

        Lemma lemma = null;
        if (candidates.size() > 0)
        {
            List<Lemma> lemmaList = db.lemmaDao.findByLemma(candidates.get(0));
            if (lemmaList.size() > 0)
                lemma = lemmaList.get(0);
        }

        return lemma;
    }

    /**
     *
     * @param idList
     * @return
     */

    static private byte[] idListToBytes(List<Integer> idList)
    {
        byte[] byteArray = new byte[idList.size() * 4];
        ByteBuffer buffer = ByteBuffer.wrap(byteArray);

        for (Integer i : idList)
            buffer.putInt(i);
        byteArray = buffer.array();

        return byteArray;
    }

    /**
     *
     * @param byteArray
     * @return
     */

    static private List<Integer> bytesToIdList(byte[] byteArray)
    {
        List<Integer> idList = new ArrayList<>();

        ByteBuffer buffer = ByteBuffer.wrap(byteArray);

        while(buffer.hasRemaining())
        {
            idList.add(buffer.getInt());
        }

        return idList;
    }

    /* Inner class ****************************************************************************************************/

    /**
     *
     */

    private class TextContentComparator implements Comparator<TextContent>
    {
        @Override
        public int compare(TextContent t1, TextContent t2)
        {
            return t1.getLocation() - t2.getLocation();
        }
    }
}