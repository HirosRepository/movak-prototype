package com.hiro.movak.model.services;

import com.hiro.movak.model.database.Database;
import com.hiro.movak.model.entities.Construction;
import org.hibernate.Transaction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class LanguageService
{
    private Database db;

    public LanguageService()
    {
        db = Database.getInstance();
    }

    public void loadConstructions(String url)
    {
        JSONParser jsonParser = new JSONParser();

        try
        {
            db.openSession();
            Transaction transaction = db.getTransaction();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(url));
            JSONArray jsonArray = (JSONArray) jsonObject.get("constructions");

            for (JSONObject construction : (Iterable<JSONObject>) jsonArray)
            {
                String name = (String) construction.get("name");
                String lemmaRegex = (String) construction.get("lemma");
                JSONObject forms = (JSONObject) construction.get("forms");

                for (String key : (Iterable<String>) forms.keySet())
                {
                    String constructionName = name + key;
                    String formRegex = (String) forms.get(key);

                    db.constructionDao.save(new Construction(constructionName, lemmaRegex, formRegex, null));
                }
            }

            transaction.commit();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            db.closeSession();
        }
    }
}
