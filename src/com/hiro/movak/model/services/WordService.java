package com.hiro.movak.model.services;

import com.hiro.movak.model.database.Database;
import com.hiro.movak.model.entities.*;
import javafx.util.Pair;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author hiro
 */

public class WordService
{
    /* Private member variables ***************************************************************************************/

    private Database db;

    /* Public methods *************************************************************************************************/

    /**
     *
     */

    public WordService()
    {
        db = Database.getInstance();
    }

    /**
     *
     * @param wordForm
     * @param closeSession
     * @return
     */

    public List<Pair<String, String>> getLemmaCandidates(String wordForm, boolean closeSession)
    {
        if (!db.sessionOpen())
            db.openSession();

        List<Construction> constructionList = db.constructionDao.findAll();

        List<Pair<String, String>> candidates = new ArrayList<>();
        for (Construction construction : constructionList)
        {
            if (wordForm.matches(construction.getFormRegex()))
            {
                String lemmaString = getLemmaString(wordForm, construction.getLemmaRegex(), construction.getFormRegex());
                String constructionName = construction.getName();
                Pair pair = new Pair(lemmaString, constructionName);
                candidates.add(pair);
            }
        }

        if (closeSession)
            db.closeSession();

        return candidates;
    }

    /**
     *
     * @param lemmaString
     * @param closeSession
     */

    public Lemma createLemma(String lemmaString, boolean closeSession)
    {
        if (!db.sessionOpen())
            db.openSession();
        Transaction transaction = db.getTransaction();

        Lemma lemma = new Lemma(lemmaString, null);
        db.lemmaDao.update(lemma);

        transaction.commit();
        if (closeSession)
            db.closeSession();
        System.out.println("Lemma saved");

        return lemma;
    }

    /**
     *
     * @param lemmaId
     * @param definition
     * @param closeSession
     * @return
     */

    public com.hiro.movak.model.entities.Pair createPair(int lemmaId, String definition, boolean closeSession)
    {
        if (!db.sessionOpen())
            db.openSession();
        Transaction transaction = db.getTransaction();

        Lemma lemma = db.lemmaDao.find(lemmaId);
        com.hiro.movak.model.entities.Pair pair = new com.hiro.movak.model.entities.Pair(lemma, definition);

        User user = db.userDao.findByName("default").get(0);
        UserPairDetails userPairDetails = new UserPairDetails(pair, user, 0.0f);

        db.pairDao.save(pair);
        db.userPairDetailsDao.save(userPairDetails);

        transaction.commit();
        if (closeSession)
            db.closeSession();
        System.out.println("Pair saved");

        return pair;
    }

    /**
     *
     * @param form
     * @param lemmaRegex
     * @param formRegex
     * @return
     */

    public String getLemmaString(String form, String lemmaRegex, String formRegex)
    {
        String lemmaSuffix = findSuffix(lemmaRegex);
        form = removeSuffix(form, formRegex);

        String lemmaString = form + lemmaSuffix;

        return lemmaString;
    }

    public List<String> getInflexions(String lemmaString)
    {
        List<String> inflexions = new ArrayList<>();
        List<Construction> constructions = db.constructionDao.findAll();

        for (Construction construction : constructions)
        {
            String wordForm = inflect(lemmaString, construction.getLemmaRegex(), construction.getFormRegex());
            inflexions.add(wordForm);
        }

        return inflexions;
    }

    /* Private methods ************************************************************************************************/

    /**
     *
     * @param word
     * @param regex
     * @return
     */

    private String removeSuffix(String word, String regex)
    {
        regex = regex.replaceAll("\\.", "");
        regex = regex.replaceAll("\\+", "");

        word = word.replaceAll(regex, "");
        return word;
    }

    /**
     *
     * @param regex
     * @return
     */

    private String findSuffix(String regex)
    {
        String suffix = "";

        Pattern r = Pattern.compile("\\.\\+[^\\.]+\\$"); // finds suffix
        Matcher m = r.matcher(regex);
        if (m.find())
        {
            suffix = m.group();
            suffix = suffix.replaceAll("\\.", "");
            suffix = suffix.replaceAll("\\+", "");
            suffix = suffix.replaceAll("\\$", "");
        }

        return suffix;
    }

    /**
     *
     * @param word
     * @param lemmaRegex
     * @param formRegex
     * @return
     */

    private String inflect(String word, String lemmaRegex, String formRegex)
    {
        String stem = removeSuffix(word, lemmaRegex);
        String formSuffix = findSuffix(formRegex);

        return stem + formSuffix;
    }
}
