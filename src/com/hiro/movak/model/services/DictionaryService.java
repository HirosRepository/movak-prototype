package com.hiro.movak.model.services;

import com.hiro.movak.model.database.Database;
import com.hiro.movak.model.entities.Lemma;
import org.hibernate.Transaction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class DictionaryService
{
    private Database db;

    public DictionaryService()
    {
        db = Database.getInstance();
    }

    public void loadLemmata(String url)
    {
        List<String> lemmaList = getListFromJson(url);

        db.openSession();
        Transaction transaction = db.getTransaction();
        for (String lemmaString : lemmaList)
        {
            Lemma lemma = new Lemma(lemmaString, null);
            db.lemmaDao.save(lemma);
        }

        transaction.commit();
        db.closeSession();
    }

    private List<String> getListFromJson(String url)
    {
        JSONParser jsonParser = new JSONParser();
        List<String> stringList = new ArrayList<>();

        try
        {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(url));
            JSONArray jsonArray = (JSONArray) jsonObject.get("lemmata");


            for (String string : (Iterable<String>) jsonArray)
            {
                stringList.add(string);
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return stringList;
    }
}
