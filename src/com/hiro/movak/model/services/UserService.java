package com.hiro.movak.model.services;

import com.hiro.movak.model.database.Database;
import com.hiro.movak.model.entities.User;
import org.hibernate.Transaction;

import java.util.List;

/**
 * @author hiro
 */

public class UserService
{
    private Database db;

    /**
     *
     */

    public UserService()
    {
        db = Database.getInstance();
    }

    /**
     *
     * @param name
     * @return Returns true if new user created
     */

    public boolean createUser(String name)
    {
        db.openSession();

        List<User> users = db.userDao.findByName(name);
        if (!users.isEmpty())
            return false;

        Transaction transaction = db.getTransaction();
        User user = new User(name);
        db.userDao.save(user);
        transaction.commit();

        db.closeSession();
        return true;
    }

    /**
     *
     */

    public void setDefaultUser()
    {
        db.openSession();

        boolean exists = db.userDao.checkExistenceByName("default");

        if (!exists)
            createUser("default");

        db.closeSession();
    }
}
