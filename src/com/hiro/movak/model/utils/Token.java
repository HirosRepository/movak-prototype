package com.hiro.movak.model.utils;

import com.hiro.movak.controllers.ReaderViewController;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

/**
 * @author hiro
 */

public class Token extends Text
{
    public enum Type
    {
        SPACE,
        PUNCTUATION,
        UNKNOWN,
        LEARNING
    }

    private ReaderViewController readerViewController = null;

    private String name;
    private Type type;
    private float familiarity;  // between 0 and 1, 0 means completely new, 1 very familiar
    private int location;

    public Token(String name, int location)
    {
        this(name, Type.SPACE, location);
    }

    /**
     *
     * @param name
     * @param type
     * @param location
     */

    public Token(String name, Type type, int location)
    {
        this(name, type, 0.0f, location);
    }

    /**
     *
     * @param name
     * @param type
     * @param familiarity
     * @param location
     */

    public Token(String name, Type type, float familiarity, int location)
    {
        super(name);

        this.name = name;
        this.type = type;
        this.familiarity = familiarity;
        this.location = location;

        setOnMousePressed(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                if (readerViewController != null)
                    readerViewController.onTokenClicked(Token.this);
            }
        });
    }

    /**
     *
     * @return
     */

    public Type getType()
    {
        return type;
    }

    /**
     *
     * @return
     */

    public String getName()
    {
        return name;
    }

    /**
     *
     * @param readerViewController
     */

    public void setReaderViewController(ReaderViewController readerViewController)
    {
        this.readerViewController = readerViewController;
    }

    /**
     *
     * @return
     */

    public int getLocation()
    {
        return location;
    }

    /**
     *
     * @param location
     */

    public void setLocation(int location)
    {
        this.location = location;
    }
}
