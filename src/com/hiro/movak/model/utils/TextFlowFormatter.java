package com.hiro.movak.model.utils;

import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.text.TextFlow;

import java.util.List;

/**
 * @author hiro
 */

public class TextFlowFormatter
{
    private TextFlow textFlow;
    private StringBuilder colorBuilder;
    private StringBuilder boundsBuilder;

    public TextFlowFormatter() { }

    /**
     *
     * @param textFlow
     * @param tokens
     * @return
     */

    public String getCss(TextFlow textFlow, List<Token> tokens)
    {
        this.textFlow = textFlow;

        colorBuilder = new StringBuilder();
        boundsBuilder = new StringBuilder();

        Bounds textBounds = this.textFlow.getBoundsInLocal();

        ObservableList<Node> nodes = this.textFlow.getChildrenUnmodifiable();
        for (int i = 0; i < nodes.size(); i++)
        {
            Token token = (Token) nodes.get(i);

            // Set colors
            getColor(token);

            // Set insets
            getInsets(textBounds, token.getBoundsInParent());

            // Comma separate
            if (i < nodes.size() - 1)
            {
                colorBuilder.append(", ");
                boundsBuilder.append(", ");
            }
        }

        String css = "-fx-background-color: " + colorBuilder.toString() + "; "
                + "-fx-background-insets: " + boundsBuilder.toString() + "; ";

        return css;
    }

    /**
     *
     * @param token
     */

    private void getColor(Token token)
    {
        if (token.getType() == Token.Type.LEARNING)
            colorBuilder.append("#ff9999");
        else if (token.getType() == Token.Type.UNKNOWN)
            colorBuilder.append("#b3ccff");
        else
            colorBuilder.append("#ffffff");
    }

    /**
     *
     * @param textBounds
     * @param tokenBounds
     */

    private void getInsets(Bounds textBounds, Bounds tokenBounds)
    {
        boundsBuilder.append(tokenBounds.getMinY()).append(" ");  // top margin
        boundsBuilder.append(Math.min(textBounds.getWidth(),
                textBounds.getMaxX() - tokenBounds.getMaxX())).append(" ");    // right margin
        boundsBuilder.append(Math.min(textBounds.getHeight(),
                textBounds.getMaxY() - tokenBounds.getMaxY())).append(" ");    // bottom margin
        boundsBuilder.append(tokenBounds.getMinX());  // left margin
    }
}
