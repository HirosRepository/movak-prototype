package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "FORM")
public class Form
{
    @Id @GeneratedValue
    @Column (name = "form_id")
    private int formId;

    @Column (name = "form", unique = true)
    private String form;

    @ManyToOne
    private Construction construction;

    @ManyToOne
    private Lemma lemma;

    public Form() { }

    public Form(String form, Construction construction, Lemma lemma)
    {
        this.form = form;
        this.construction = construction;
        this.lemma = lemma;
    }

    public int getFormId()
    {
        return formId;
    }

    public void setFormId(int formId)
    {
        this.formId = formId;
    }

    public String getForm()
    {
        return form;
    }

    public void setForm(String form)
    {
        this.form = form;
    }

    public Construction getConstruction()
    {
        return construction;
    }

    public void setConstruction(Construction construction)
    {
        this.construction = construction;
    }

    public Lemma getLemma()
    {
        return lemma;
    }

    public void setLemma(Lemma lemma)
    {
        this.lemma = lemma;
    }
}
