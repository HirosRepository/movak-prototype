package com.hiro.movak.model.entities;
import javax.persistence.*;

@Entity
@Table (name = "USER_PAIR_DETAILS")
public class UserPairDetails
{
    @Id @GeneratedValue
    @Column (name = "user_pair_details_id")
    private int userPairDetailsId;

    @OneToOne
    private Pair pair;

    @OneToOne
    private User user;

    @Column (name = "familiarity")
    private float familiarity;

    // todo: private Date dateCreated;
    // todo: private Date last_learned;

    public UserPairDetails() { }

    public UserPairDetails(Pair pair, User user, float familiarity)
    {
        this.pair = pair;
        this.user = user;
        this.familiarity = familiarity;
    }

    public int getUserPairDetailsId()
    {
        return userPairDetailsId;
    }

    public void setUserPairDetailsId(int userPairDetailsId)
    {
        this.userPairDetailsId = userPairDetailsId;
    }

    public Pair getPair()
    {
        return pair;
    }

    public void setPair(Pair pair)
    {
        this.pair = pair;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public float getFamiliarity()
    {
        return familiarity;
    }

    public void setFamiliarity(float familiarity)
    {
        this.familiarity = familiarity;
    }
}
