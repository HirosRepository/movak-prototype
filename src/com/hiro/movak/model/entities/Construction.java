package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "CONSTRUCTION")
public class Construction
{
    @Id @GeneratedValue
    @Column (name = "construction_id")
    private int constructionId;

    @Column (name = "name")
    private String name;

    @Column (name = "lemma_regex")
    private String lemmaRegex;

    @Column (name = "form_regex")
    private String formRegex;

    @OneToOne
    private Language language;

    public Construction() { }

    public Construction(String name, String lemmaRegex, String formRegex, Language language)
    {
        this.name = name;
        this.lemmaRegex = lemmaRegex;
        this.formRegex = formRegex;
        this.language = language;
    }

    public int getConstructionId()
    {
        return constructionId;
    }

    public void setConstructionId(int constructionId)
    {
        this.constructionId = constructionId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLemmaRegex()
    {
        return lemmaRegex;
    }

    public void setLemmaRegex(String lemmaRegex)
    {
        this.lemmaRegex = lemmaRegex;
    }

    public String getFormRegex()
    {
        return formRegex;
    }

    public void setFormRegex(String formRegex)
    {
        this.formRegex = formRegex;
    }

    public Language getLanguage()
    {
        return language;
    }

    public void setLanguage(Language language)
    {
        this.language = language;
    }
}