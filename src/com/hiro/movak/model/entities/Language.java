package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "LANGUAGE")
public class Language
{
    @Id @GeneratedValue
    @Column (name = "language_id")
    private int languageId;

    @Column (name = "name")
    private String name;

    @Column (name = "num_lemmata")
    private int numLemmata;

    @Column (name = "num_constructions")
    private int numConstructions;

    @Column (name = "num_pairs")
    private int numPairs;

    public Language() { }

    public Language(String name, int numLemmata, int numConstructions, int numPairs)
    {
        this.name = name;
        this.numLemmata = numLemmata;
        this.numConstructions = numConstructions;
        this.numPairs = numPairs;
    }

    public int getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(int languageId)
    {
        this.languageId = languageId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getNumLemmata()
    {
        return numLemmata;
    }

    public void setNumLemmata(int numLemmata)
    {
        this.numLemmata = numLemmata;
    }

    public int getNumConstructions()
    {
        return numConstructions;
    }

    public void setNumConstructions(int numConstructions)
    {
        this.numConstructions = numConstructions;
    }

    public int getNumPairs()
    {
        return numPairs;
    }

    public void setNumPairs(int numPairs)
    {
        this.numPairs = numPairs;
    }
}