package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table(name = "USER")
public class User
{
    @Id @GeneratedValue
    @Column (name = "user_id")
    private int userId;

    @Column (name = "user_name")
    private String userName;

    public User() { }

    public User(String userName)
    {
        this.userName = userName;
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }
}