package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "LEMMA")
public class Lemma
{
    @Id @GeneratedValue
    @Column (name = "lemma_id")
    private int lemmaId;

    @Column (name = "lemma")
    private String lemma;

    @OneToOne
    private Language language;

    public Lemma() { }

    public Lemma(String lemma, Language language)
    {
        this.lemma = lemma;
        this.language = language;
    }

    public int getLemmaId()
    {
        return lemmaId;
    }

    public void setLemmaId(int lemmaId)
    {
        this.lemmaId = lemmaId;
    }

    public String getLemma()
    {
        return lemma;
    }

    public void setLemma(String lemma)
    {
        this.lemma = lemma;
    }

    public Language getLanguage()
    {
        return language;
    }

    public void setLanguage(Language language)
    {
        this.language = language;
    }
}