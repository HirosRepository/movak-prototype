package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table(name = "USER_TEXT_DETAILS")
public class UserTextDetails
{
    @Id @GeneratedValue
    @Column (name = "user_text_details")
    private int userTextDetailsId;

    @OneToOne
    private User user;

    @OneToOne
    private Text text;

    @Column (name = "last_location")
    private int lastLocation;

    @Column (name = "progress")
    private float progress;

    @Column (name = "known_words")
    private float knownWords;

    public UserTextDetails() { }

    public UserTextDetails(User user, Text text, int lastLocation, float progress, float knownWords)
    {
        this.user = user;
        this.text = text;
        this.lastLocation = lastLocation;
        this.progress = progress;
        this.knownWords = knownWords;
    }

    public int getUserTextDetailsId()
    {
        return userTextDetailsId;
    }

    public void setUserTextDetailsId(int userTextDetailsId)
    {
        this.userTextDetailsId = userTextDetailsId;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Text getText()
    {
        return text;
    }

    public void setText(Text text)
    {
        this.text = text;
    }

    public int getLastLocation()
    {
        return lastLocation;
    }

    public void setLastLocation(int lastLocation)
    {
        this.lastLocation = lastLocation;
    }

    public float getProgress()
    {
        return progress;
    }

    public void setProgress(float progress)
    {
        this.progress = progress;
    }

    public float getKnownWords()
    {
        return knownWords;
    }

    public void setKnownWords(float knownWords)
    {
        this.knownWords = knownWords;
    }
}
