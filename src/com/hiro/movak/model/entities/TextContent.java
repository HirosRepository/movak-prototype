package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "TEXT_CONTENT")
public class TextContent
{
    @Id @GeneratedValue
    @Column (name = "text_content_id")
    private int textContentId;

    @ManyToOne
    private Text text;

    @Column (name = "location")
    private int location;

    @ManyToOne
    private Form form;

    @ManyToOne
    private Pair pair;

    public TextContent() { }

    public TextContent(Text text, int location, Form form, Pair pair)
    {
        this.text = text;
        this.location = location;
        this.form = form;
        this.pair = pair;
    }

    public int getTextContentId()
    {
        return textContentId;
    }

    public void setTextContentId(int textContentId)
    {
        this.textContentId = textContentId;
    }

    public Text getText()
    {
        return text;
    }

    public void setText(Text text)
    {
        this.text = text;
    }

    public int getLocation()
    {
        return location;
    }

    public void setLocation(int location)
    {
        this.location = location;
    }

    public Form getForm()
    {
        return form;
    }

    public void setForm(Form form)
    {
        this.form = form;
    }

    public Pair getPair()
    {
        return pair;
    }

    public void setPair(Pair pair)
    {
        this.pair = pair;
    }
}