package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "PAIR")
public class Pair
{
    @Id @GeneratedValue
    @Column (name = "pair_id")
    private int pairId;

    @ManyToOne
    private Lemma lemma;

    @Column (name = "definition")
    private String definition;

    public Pair() { }

    public Pair(Lemma lemma, String definition)
    {
        this.lemma = lemma;
        this.definition = definition;
    }

    public int getPairId()
    {
        return pairId;
    }

    public void setPairId(int pairId)
    {
        this.pairId = pairId;
    }

    public Lemma getLemma()
    {
        return lemma;
    }

    public void setLemma(Lemma lemma)
    {
        this.lemma = lemma;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }
}
