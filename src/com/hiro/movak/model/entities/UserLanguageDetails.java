package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "USER_LANGUAGE_DETAILS")
public class UserLanguageDetails
{
    @Id @GeneratedValue
    @Column (name = "user_language_details_id")
    private int userLanguageDetailsId;

    @OneToOne
    private User user;

    @OneToOne
    private Language language;

    @Column (name = "num_known_lemmata")
    private int numKnownLemmata;

    @Column (name = "num_known_constructions")
    private int numKnownConstructions;

    @Column (name = "num_known_pairs")
    private int numKnownPairs;

    public UserLanguageDetails() { }

    public UserLanguageDetails(User user,
                               Language language,
                               int numKnownLemmata,
                               int numKnownConstructions,
                               int numKnownPairs)
    {
        this.user = user;
        this.language = language;
        this.numKnownLemmata = numKnownLemmata;
        this.numKnownConstructions = numKnownConstructions;
        this.numKnownPairs = numKnownPairs;
    }

    public int getUserLanguageDetailsId()
    {
        return userLanguageDetailsId;
    }

    public void setUserLanguageDetailsId(int userLanguageDetailsId)
    {
        this.userLanguageDetailsId = userLanguageDetailsId;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Language getLanguage()
    {
        return language;
    }

    public void setLanguage(Language language)
    {
        this.language = language;
    }

    public int getNumKnownLemmata()
    {
        return numKnownLemmata;
    }

    public void setNumKnownLemmata(int numKnownLemmata)
    {
        this.numKnownLemmata = numKnownLemmata;
    }

    public int getNumKnownConstructions()
    {
        return numKnownConstructions;
    }

    public void setNumKnownConstructions(int numKnownConstructions)
    {
        this.numKnownConstructions = numKnownConstructions;
    }

    public int getNumKnownPairs()
    {
        return numKnownPairs;
    }

    public void setNumKnownPairs(int numKnownPairs)
    {
        this.numKnownPairs = numKnownPairs;
    }
}
