package com.hiro.movak.model.entities;

import javax.persistence.*;

@Entity
@Table (name = "TEXT")
public class Text
{
    @Id @GeneratedValue
    @Column (name = "text_id")
    private int textId;

    @Column (name = "title", nullable = false)
    private String title;

    @Column (name = "author")
    private String author;

    @Column (name = "language_id", nullable = false)
    private int languageId;

    @Column (name = "length", nullable = false)
    private int length;

    @Lob
    private byte[] content;

    public Text() { }

    public Text(String title, int languageId, int length)
    {
        this(title, null, languageId, length, null);
    }

    public Text(String title, String author, int languageId, int length, byte[] content)
    {
        this.title = title;
        this.author = author;
        this.languageId = languageId;
        this.length = length;
        this.content = content;
    }

    public int getTextId()
    {
        return textId;
    }

    public void setTextId(int textId)
    {
        this.textId = textId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public int getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(int languageId)
    {
        this.languageId = languageId;
    }

    public int getLength()
    {
        return length;
    }

    public void setLength(int length)
    {
        this.length = length;
    }

    public byte[] getContent()
    {
        return content;
    }

    public void setContent(byte[] content)
    {
        this.content = content;
    }
}