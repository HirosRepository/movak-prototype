package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.Form;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author hiro
 */

public class FormDao extends GenericDao<Form>
{
    /**
     *
     * @param db
     */

    public FormDao(Database db)
    {
        super(Form.class, db);
    }

    /**
     *
     * @param wordForm
     * @return
     */

    @SuppressWarnings("unchecked")
    public Form findByForm(String wordForm)
    {
        Query query = db.currentSession.createQuery("FROM Form F WHERE F.form = :word_form");
        query.setParameter("word_form", wordForm);

        assert (query.list().size() == 0 || query.list().size() == 1);

        if (query.list().size() == 1)
            return (Form) query.list().get(0);
        else
            return null;
    }
}
