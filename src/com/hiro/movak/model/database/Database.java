package com.hiro.movak.model.database;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * @author hiro
 */

public class Database
{
    // Private member variables
    private static Database db;
    private boolean isOpen = false;

    // Package member variables
    SessionFactory sessionFactory;
    Session currentSession = null;

    /* Public DAOs ****************************************************************************************************/
    public ConstructionDao constructionDao;
    public FormDao formDao;
    public LanguageDao languageDao;
    public LemmaDao lemmaDao;
    public PairDao pairDao;
    public TextDao textDao;
    public TextContentDao textContentDao;
    public UserDao userDao;
    public UserTextDetailsDao userTextDetailsDao;
    public UserPairDetailsDao userPairDetailsDao;

    /* Public methods *************************************************************************************************/

    /**
     *
     * @return
     */

    public static synchronized Database getInstance()
    {
        if (db == null)
        {
            db = new Database();
        }

        return db;
    }

    /**
     *
     */

    public void openSession()
    {
        currentSession = sessionFactory.openSession();
        isOpen = true;
    }

    /**
     *
     */

    public void closeSession()
    {
        currentSession.close();
        isOpen = false;
    }

    /**
     *
     * @return
     */

    public boolean sessionOpen()
    {
        return isOpen;
    }

    /**
     *
     * @return
     */

    public Transaction getTransaction()
    {
        return currentSession.beginTransaction();
    }

    /* Private methods ************************************************************************************************/

    /**
     *
     */

    private Database()
    {
        try
        {
            Configuration configuration = new Configuration();
            configuration.configure();

            sessionFactory = configuration.buildSessionFactory();
        }
        catch (Throwable ex)
        {
            throw new ExceptionInInitializerError(ex);
        }

        constructionDao = new ConstructionDao(this);
        formDao = new FormDao(this);
        languageDao = new LanguageDao(this);
        lemmaDao = new LemmaDao(this);
        pairDao = new PairDao(this);
        textDao = new TextDao(this);
        textContentDao = new TextContentDao(this);
        userDao = new UserDao(this);
        userTextDetailsDao = new UserTextDetailsDao(this);
        userPairDetailsDao = new UserPairDetailsDao(this);
    }
}