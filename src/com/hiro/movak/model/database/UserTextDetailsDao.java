package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.UserTextDetails;
import org.hibernate.query.Query;

/**
 * @author
 */

public class UserTextDetailsDao extends GenericDao<UserTextDetails>
{
    /**
     *
     * @param db
     */

    public UserTextDetailsDao(Database db)
    {
        super(UserTextDetails.class, db);
    }

    /**
     *
     * @param textId
     * @return
     */

    @SuppressWarnings("unchecked")
    public UserTextDetails findByTextId(int textId)
    {
        Query query = db.currentSession.createQuery("FROM UserTextDetails U WHERE U.text.textId = :text_id");
        query.setParameter("text_id", textId);

        if (query.list().isEmpty())
            return null;
        else
            return (UserTextDetails) query.list().get(0);
    }
}