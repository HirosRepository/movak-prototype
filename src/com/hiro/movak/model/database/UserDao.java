package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.User;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author hiro
 */

public class UserDao extends GenericDao<User>
{
    /**
     *
     * @param db
     */

    public UserDao(Database db)
    {
        super(User.class, db);
    }

    /**
     *
     * @param name
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<User> findByName(String name)
    {
        Query query = db.currentSession.createQuery("FROM User U WHERE U.userName = :user_name");
        query.setParameter("user_name", name);

        return query.list();
    }

    /**
     *
     * @param name
     * @return
     */

    public boolean checkExistenceByName(String name)
    {
        Query query = db.currentSession.createQuery("FROM User U WHERE U.userName = :user_name");
        query.setParameter("user_name", name);

        return !query.list().isEmpty();
    }
}