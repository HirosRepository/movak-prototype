package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.Lemma;
import com.hiro.movak.model.entities.Pair;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author hiro
 */

public class PairDao extends GenericDao<Pair>
{
    /**
     *
     * @param db
     */

    public PairDao(Database db)
    {
        super(Pair.class, db);
    }

    /**
     *
     * @param lemma
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<Pair> findByLemma(Lemma lemma)
    {
        Query query = db.currentSession.createQuery("FROM Pair P WHERE P.lemma.lemmaId = :lemma_id");
        query.setParameter("lemma_id", lemma.getLemmaId());

        return (List<Pair>) query.list();
    }
}
