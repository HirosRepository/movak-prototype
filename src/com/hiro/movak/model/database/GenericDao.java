package com.hiro.movak.model.database;

import java.util.List;

/**
 * @author hiro
 * @param <T>
 */

public abstract class GenericDao<T>
{
    protected Database db;
    private Class<T> type;

    /**
     *
     * @param type
     * @param db
     */

    GenericDao(Class<T> type, Database db)
    {
        this.db = db;
        this.type = type;
    }

    /**
     *
     * @param id
     * @return
     */

    public T find(int id)
    {
        return db.currentSession.get(type, id);
    }

    /**
     *
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<T> findAll()
    {
        return db.currentSession.createQuery("FROM " + type.getName()).list();
    }

    /**
     *
     * @param t
     * @return
     */

    public int save(T t)
    {
        return (int) db.currentSession.save(t);
    }

    /**
     *
     * @param t
     */

    public void update(T t)
    {
        db.currentSession.saveOrUpdate(t);
    }

    /**
     *
     * @param t
     */

    public void delete(T t)
    {
        db.currentSession.delete(t);
    }
}