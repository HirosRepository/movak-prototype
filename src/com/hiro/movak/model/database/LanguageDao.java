package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.Language;

/**
 * @author hiro
 */

public class LanguageDao extends GenericDao<Language>
{
    /**
     *
     * @param db
     */

    public LanguageDao(Database db)
    {
        super(Language.class, db);
    }
}
