package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.UserPairDetails;
import org.hibernate.query.Query;

/**
 * @author
 */

public class UserPairDetailsDao extends GenericDao<UserPairDetails>
{
    /**
     *
     * @param db
     */

    public UserPairDetailsDao(Database db)
    {
        super(UserPairDetails.class, db);
    }

    /**
     *
     * @param pairId
     * @return
     */

    public UserPairDetails findByPairId(int pairId)
    {
        Query query = db.currentSession.createQuery("FROM UserPairDetails U WHERE U.pair.pairId = :pair_id");
        query.setParameter("pair_id", pairId);
        return (UserPairDetails) query.list().get(0);
    }
}
