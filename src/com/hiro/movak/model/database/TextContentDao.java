package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.TextContent;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author hiro
 */

public class TextContentDao extends GenericDao<TextContent>
{
    /**
     *
     * @param db
     */

    public TextContentDao(Database db)
    {
        super(TextContent.class, db);
    }

    /**
     *
     * @param id
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<TextContent> findByTextId(int id)
    {
        Query query = db.currentSession.createQuery("FROM TextContent T WHERE T.text.id = :text_id");
        query.setParameter("text_id", id);

        return (List<TextContent>) query.list();
    }
}
