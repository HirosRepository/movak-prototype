package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.Construction;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author hiro
 */

public class ConstructionDao extends GenericDao<Construction>
{
    /**
     *
     * @param db
     */

    public ConstructionDao(Database db)
    {
        super(Construction.class, db);
    }
}
