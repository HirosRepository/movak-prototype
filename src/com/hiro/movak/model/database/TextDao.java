package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.Text;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author
 */

public class TextDao extends GenericDao<Text>
{
    /**
     *
     * @param db
     */

    public TextDao(Database db)
    {
        super(Text.class, db);
    }

    /**
     *
     * @param title
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<Text> findByTitle(String title)
    {
        Query query = db.currentSession.createQuery("FROM Text T WHERE T.title = :title");
        query.setParameter("title", title);

        return (List<Text>) query.list();
    }
}