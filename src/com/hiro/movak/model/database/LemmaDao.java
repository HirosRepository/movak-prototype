package com.hiro.movak.model.database;

import com.hiro.movak.model.entities.Lemma;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author hiro
 */

public class LemmaDao extends GenericDao<Lemma>
{
    /**
     *
     * @param db
     */

    public LemmaDao(Database db)
    {
        super(Lemma.class, db);
    }

    /**
     *
     * @param lemmaString
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<Lemma> findByLemma(String lemmaString)
    {
        Query query = db.currentSession.createQuery("FROM Lemma L WHERE L.lemma = :lemma_string");
        query.setParameter("lemma_string", lemmaString);

        return (List<Lemma>) query.list();
    }
}