package com.hiro.movak.controllers;

import com.hiro.movak.model.entities.Lemma;
import com.hiro.movak.model.entities.Pair;
import com.hiro.movak.model.services.TextService;
import com.hiro.movak.model.services.WordService;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * @author hiro
 */

public class PairViewController
{
    ReaderViewController readerViewController = null;

    @FXML
    private Label lemma_label;

    @FXML
    private TextField definition;

    private WordService wordService = new WordService();
    private TextService textService = new TextService();

    private int textId;
    private Lemma lemma;

    /**
     *
     * @param textId
     * @param lemma
     */

    public void setSourceInfo(int textId, Lemma lemma)
    {
        this.textId = textId;
        this.lemma = lemma;

        lemma_label.setText(lemma.getLemma());
    }

    /**
     *
     * @param readerViewController
     */

    public void setReaderViewController(ReaderViewController readerViewController)
    {
        this.readerViewController = readerViewController;
    }

    /**
     *
     * @param mouseEvent
     */

    public void onSubmitButtonClicked(MouseEvent mouseEvent)
    {
        System.out.println(lemma.getLemma());
        Pair pair = wordService.createPair(lemma.getLemmaId(), definition.getText(),true);
        System.out.println(pair.getLemma().getLemma());
        textService.updateTextContent(textId, pair.getPairId());
        readerViewController.clearSideBar();
        readerViewController.reloadText();
    }
}
