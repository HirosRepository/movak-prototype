package com.hiro.movak.controllers;

import com.hiro.movak.model.entities.Lemma;
import com.hiro.movak.model.services.WordService;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 * @author hiro
 */

public class LemmaSuggestionCardController
{
    /* Private member variables ***************************************************************************************/

    private LemmaViewController lemmaViewController;

    @FXML
    private GridPane root;

    @FXML
    private Label suggestion;

    @FXML
    private Label pos;

    @FXML
    private Label construction;

    // Services
    private WordService wordService = new WordService();

    // Other information
    private String lemmaString;

    /* Public methods *************************************************************************************************/

    /**
     *
     * @param pos
     */

    public void setPos(String pos)
    {
        this.pos.setText(pos);
    }

    /**
     *
     * @param lemmaString
     * @param constructionName
     */

    public void setSuggestion(String lemmaString, String constructionName)
    {
        this.lemmaString = lemmaString;
        this.suggestion.setText(lemmaString);
        this.construction.setText(constructionName);
    }

    /**
     *
     * @param lemmaViewController
     */

    public void setLemmaViewController(LemmaViewController lemmaViewController)
    {
        this.lemmaViewController = lemmaViewController;
    }

    /* Event callbacks ************************************************************************************************/

    /**
     *
     * @param mouseEvent
     */

    public void onCardClicked(MouseEvent mouseEvent)
    {
        Lemma lemma = wordService.createLemma(lemmaString, true);
        lemmaViewController.closeView(lemma);
    }
}
