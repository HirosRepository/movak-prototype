package com.hiro.movak.controllers;

import com.hiro.movak.model.entities.Text;
import com.hiro.movak.model.services.TextService;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 * @author hiro
 */

public class AddBookDialogController
{
    /* Private member variables ***************************************************************************************/

    private LibraryViewController libraryViewController;

    @FXML
    private Label dialog_title;

    @FXML
    private TextField text_title;

    @FXML
    private ComboBox language;

    @FXML
    private Label url;

    @FXML
    private Button choose_file_button;

    @FXML
    private Button add_button;

    @FXML
    private Button cancel_button;

    // Services
    private TextService textService = new TextService();

    // Other information
    private String enteredTitle = "";
    private File textFile = null;

    /* Public methods *************************************************************************************************/

    /**
     *
     */

    @FXML
    public void initialize()
    {
        text_title.textProperty().addListener((observable, oldValue, newValue) -> { enteredTitle = newValue; });
    }

    /**
     *
     * @param libraryViewController
     */

    public void setLibraryViewController(LibraryViewController libraryViewController)
    {
        this.libraryViewController = libraryViewController;
    }

    /* Event callbacks ************************************************************************************************/

    /**
     *
     * @param mouseEvent
     */

    public void onChooseFileButtonClicked(MouseEvent mouseEvent)
    {
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null);

        String fileName = "";
        if (selectedFile != null)
        {
            fileName = selectedFile.getName();

            if (!selectedFile.getName().endsWith(".txt"))
            {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Invalid file extension");
                alert.setHeaderText(null);
                alert.setContentText("The text file needs to have .txt extension.");

                alert.showAndWait();
            }
            else
            {
                url.setText(selectedFile.getName());
                textFile = selectedFile;
            }
        }
        else
        {
            url.setText("Choose a file...");
        }
    }

    /**
     *
     * @param mouseEvent
     */

    public void onAddButtonClicked(MouseEvent mouseEvent)
    {
        if (enteredTitle.equals(""))
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Title not set");
            alert.setHeaderText(null);
            alert.setContentText("Please enter a title");

            alert.showAndWait();
            return;
        }

        if (textFile == null)
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("File not specified");
            alert.setHeaderText(null);
            alert.setContentText("Please select a file");

            alert.showAndWait();
            return;
        }

        Text newText = new Text(enteredTitle, 0, 0);
        textService.createText(newText, textFile);
        Stage stage = (Stage) add_button.getScene().getWindow();
        stage.close();
        libraryViewController.updateLibraryContent();
    }

    /**
     *
     * @param mouseEvent
     */

    public void onCancelButtonClicked(MouseEvent mouseEvent)
    {
        Stage stage = (Stage) cancel_button.getScene().getWindow();
        stage.close();
    }
}
