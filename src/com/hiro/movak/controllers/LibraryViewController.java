package com.hiro.movak.controllers;

import com.hiro.movak.model.entities.Text;
import com.hiro.movak.model.services.TextService;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hiro
 */

public class LibraryViewController
{
    /* Private member variables ***************************************************************************************/

    private MainController mainController;

    private static final int NUM_CARD_ROWS = 2;
    private static final int NUM_CARD_COLS = 3;

    @FXML
    private Label title;

    @FXML
    private VBox side_bar;

    @FXML
    private ScrollPane shelf_pane;

    @FXML
    private ListView<Label> book_shelf;

    @FXML
    private GridPane card_area;

    // Services
    private TextService textService = new TextService();

    private ObservableList<Text> texts = FXCollections.observableArrayList();
    private List<BookCardController> bookCards = new ArrayList<>();

    /* Public methods *************************************************************************************************/

    /**
     *
     */

    @FXML
    public void initialize()
    {
        List<Text> texts = textService.getAllTexts();

        // Add to the card list
        for (int row = 0; row < NUM_CARD_ROWS; row++)
        {
            for (int col = 0; col < NUM_CARD_COLS; col++)
            {
                if (texts.size() > row * NUM_CARD_COLS + col)
                    loadBookCard(row, col, texts.get(row * NUM_CARD_COLS + col));
                else
                    loadAddBookCard(row, col);
            }
        }

        book_shelf.getItems().addListener(new ListChangeListener<Label>()
        {
            @Override
            public void onChanged(Change<? extends Label> change) { }
        });

        updateLibraryContent();
    }

    /**
     *
     * @param mainController
     */

    public void setConnection(MainController mainController)
    {
        this.mainController = mainController;

        // Set up a connection to the BookCardController class
        for (BookCardController controller : bookCards)
        {
            controller.setConnection(mainController);
        }
    }

    /**
     *
     */

    public void updateLibraryContent()
    {
        book_shelf.getItems().clear();
        texts.clear();
        texts.addAll(textService.getAllTexts());
        for (Text text : texts)
        {
            Label label = new Label(text.getTitle());
            book_shelf.getItems().add(label);
        }
    }

    public void moveCardIntoLibrary(GridPane bookCard)
    {
        int col = GridPane.getColumnIndex(bookCard);
        int row = GridPane.getRowIndex(bookCard);
        card_area.getChildren().remove(bookCard);
        loadAddBookCard(row, col);
    }

    /* Event callbacks ************************************************************************************************/

    /**
     *
     * @param mouseEvent
     */

    public void onAddButtonClicked(MouseEvent mouseEvent)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/add_book_dialog.fxml"));
            GridPane addDialog = (GridPane) loader.load();
            AddBookDialogController addBookDialogController = loader.getController();
            addBookDialogController.setLibraryViewController(this);
            addDialog.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());

            Stage stage = new Stage();
            stage.setScene(new Scene(addDialog));
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initOwner(((Node)mouseEvent.getSource()).getScene().getWindow());
            stage.show();
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /* Private methods ************************************************************************************************/

    /**
     *
     * @param row
     * @param col
     * @param text
     */

    private void loadBookCard(int row, int col, Text text)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/book_card/book_card.fxml"));
            GridPane bookCard = (GridPane) loader.load();
            bookCard.hoverProperty().addListener(l->{});
            bookCard.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());
            BookCardController bookCardController = loader.getController();
            bookCardController.setLibraryViewController(this);
            bookCardController.setEventHandler(shelf_pane);
            bookCards.add(bookCardController);
            bookCards.get(row * NUM_CARD_COLS + col).setText(text);

            card_area.add(bookCard, col, row);
        }

        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     * @param row
     * @param col
     */

    private void loadAddBookCard(int row, int col)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/book_card/add_book.fxml"));
            GridPane addBookCard = (GridPane) loader.load();
            addBookCard.hoverProperty().addListener(l->{});
            addBookCard.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());

            card_area.add(addBookCard, col, row);
            addBookCard.toBack();
        }

        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
