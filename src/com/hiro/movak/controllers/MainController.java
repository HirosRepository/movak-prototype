package com.hiro.movak.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.io.IOException;

/**
 * Main controller
 * @author hiro
 */

public class MainController
{
    /* Private member variables ***************************************************************************************/
    private enum ViewType
    {
        DASHBOARD_VIEW,
        LIBRARY_VIEW,
        LEARN_VIEW,
        STATS_VIEW,
        LANGUAGES_VIEW
    }

    private Pane currentView;

    @FXML
    private GridPane root;

    @FXML
    private StackPane work_area;

    @FXML
    private GridPane menu_bar;

    // Stacked panes
    private AnchorPane dashboard_view;
    private GridPane library_view;
    private GridPane learn_view;
    private AnchorPane stats_view;
    private AnchorPane languages_view;

    // Navigation buttons
    @FXML
    private Pane dashboard_nav_btn;

    @FXML
    private Pane library_nav_btn;

    @FXML
    private Pane learn_nav_btn;

    @FXML
    private Pane stats_nav_btn;

    @FXML
    private Pane languages_nav_btn;

    /* Public methods *************************************************************************************************/

    /**
     * Load all views and set up listeners for buttons
     */

    @FXML
    public void initialize()
    {
        try
        {
            FXMLLoader loader;

            // Load menu bar
            loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/menu_bar.fxml"));
            GridPane menu_bar = (GridPane) loader.load();
            menu_bar.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());
            root.add(menu_bar, 1, 0);

            // Add dashboard pane
            loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/dashboard_view.fxml"));
            dashboard_view = (AnchorPane) loader.load();
            dashboard_view.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());
            work_area.getChildren().add(dashboard_view);

            // Add library pane
            loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/library_view.fxml"));
            library_view = (GridPane) loader.load();
            library_view.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());
            LibraryViewController libraryViewController = loader.getController();
            libraryViewController.setConnection(this);
            work_area.getChildren().add(library_view);

            // Add learn pane
            loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/learn_view.fxml"));
            learn_view = (GridPane) loader.load();
            learn_view.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());
            work_area.getChildren().add(learn_view);

            // Add stats pane
            loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/stats_view.fxml"));
            stats_view = (AnchorPane) loader.load();
            stats_view.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());
            work_area.getChildren().add(stats_view);

            // Add languages pane
            loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/languages_view.fxml"));
            languages_view = (AnchorPane) loader.load();
            languages_view.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());
            work_area.getChildren().add(languages_view);

            // Go to dashboard on start
            goToView(ViewType.DASHBOARD_VIEW);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        // Set hover properties
        dashboard_nav_btn.hoverProperty().addListener(l->{});
        library_nav_btn.hoverProperty().addListener(l->{});
        learn_nav_btn.hoverProperty().addListener(l->{});
        stats_nav_btn.hoverProperty().addListener(l->{});
        languages_nav_btn.hoverProperty().addListener(l->{});
    }

    /**
     * Adds a new pane to the work_area pane from other controllers
     * @param pane new pane to be added
     */

    public void setWorkArea(Pane pane)
    {
        // question: is adding good idea (list gets too big?)
        work_area.getChildren().add(pane);
    }

    /* Event callbacks ************************************************************************************************/

    /**
     * Go to the dashboard view
     * @param mouseEvent
     */

    public void onDashboardNavButtonClicked(MouseEvent mouseEvent)
    {
        goToView(ViewType.DASHBOARD_VIEW);
    }

    /**
     * Go to the library view
     * @param mouseEvent
     */

    public void onLibraryNavButtonClicked(MouseEvent mouseEvent)
    {
        goToView(ViewType.LIBRARY_VIEW);
    }

    /**
     * Go to the learn view
     * @param mouseEvent
     */

    public void onLearnNavButtonClicked(MouseEvent mouseEvent)
    {
        goToView(ViewType.LEARN_VIEW);
    }

    /**
     * Go to the stats view
     * @param mouseEvent
     */

    public void onStatsNavButtonClicked(MouseEvent mouseEvent)
    {
        goToView(ViewType.STATS_VIEW);
    }

    /**
     * Go to the languages view
     * @param mouseEvent
     */

    public void onLanguagesNavButton(MouseEvent mouseEvent)
    {
        goToView(ViewType.LANGUAGES_VIEW);
    }

    /* Private methods ************************************************************************************************/

    /**
     * Go to the specified view
     * @param view target view to go to
     */

    private void goToView(ViewType view)
    {

        switch (view)
        {
            case DASHBOARD_VIEW:
                currentView = dashboard_view;
                break;
            case LIBRARY_VIEW:
                currentView = library_view;
                break;
            case LEARN_VIEW:
                currentView = learn_view;
                break;
            case STATS_VIEW:
                currentView = stats_view;
                break;
            case LANGUAGES_VIEW:
                currentView = languages_view;
                break;
        }

        ObservableList<Node> nodes = work_area.getChildren();
        int index = 0;
        for (int i = 0; i < nodes.size(); i++)
        {
            if (nodes.get(i) == currentView)
                index = i;
        }
        nodes.get(index).toFront();
    }
}
