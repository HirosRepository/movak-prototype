package com.hiro.movak.controllers;

import com.hiro.movak.model.entities.Text;
import com.hiro.movak.model.entities.UserTextDetails;
import com.hiro.movak.model.services.TextService;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.io.IOException;

/**
 * @author hiro
 */

public class BookCardController
{
    /* Private member variables ***************************************************************************************/

    private MainController mainController;
    private LibraryViewController libraryViewController;

    private TextService textService = new TextService();

    @FXML
    private GridPane book_card;

    @FXML
    private Label title;

    @FXML
    private Label progress_label;

    @FXML
    private ProgressBar progress_bar;

    // Other information
    private Text text = null;
    private UserTextDetails userTextDetails = null;

    /* Public methods *************************************************************************************************/

    /**
     *
     */

    @FXML
    public void initialize()
    {
        // Set property listeners to sync height and width

        book_card.heightProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1)
            {
                book_card.setPrefHeight(t1.doubleValue());
                book_card.setPrefWidth(t1.doubleValue());
            }
        });

        book_card.widthProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1)
            {
                book_card.setPrefWidth(t1.doubleValue());
                book_card.setPrefWidth(t1.doubleValue());
            }
        });
    }

    public void setLibraryViewController(LibraryViewController libraryViewController)
    {
        this.libraryViewController = libraryViewController;
    }

    /**
     *
     * @param controller
     */

    public void setConnection(MainController controller)
    {
        this.mainController = controller;
    }

    /*public void setTitle(String title)
    {
        this.title.setText(title);
    }*/

    /**
     *
     * @param text
     */

    public void setText(Text text)
    {
        this.text = text;
        title.setText(text.getTitle());
        updateProgress();
    }

    public void setEventHandler(ScrollPane target)
    {
        book_card.addEventHandler(MouseEvent.ANY, new CardEventHandler(e -> { }, e -> { }, book_card, target));
    }

    /* Event callbacks ************************************************************************************************/

    /**
     *
     * @param mouseEvent
     */

    public void onCardClicked(MouseEvent mouseEvent)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/reader_view.fxml"));
            GridPane reader_view = (GridPane) loader.load();
            ReaderViewController readerViewController = loader.getController();
            readerViewController.setTextId(textService.getTextId(title.getText()));
            mainController.setWorkArea(reader_view);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /* Private methods ************************************************************************************************/

    /**
     *
     */

    private void updateProgress()
    {
        userTextDetails = textService.getUserTextDetails(text.getTextId());
        if (userTextDetails != null)
        {
            progress_label.setText("Progress: " + (int)(userTextDetails.getProgress() * 100) + "%");
            progress_bar.setProgress(userTextDetails.getProgress());
        }
    }

    /* Inner classes **************************************************************************************************/

    /**
     *
     */

    private DragContext dragContext = new DragContext();
    private class DragContext
    {
        public double anchorX;
        public double anchorY;
    }

    private class CardEventHandler implements EventHandler<MouseEvent>
    {
        private final EventHandler<MouseEvent> onDraggedEventHandler;
        private final EventHandler<MouseEvent> onClickedEventHandler;
        private final GridPane card;
        private final ScrollPane targetArea;

        private boolean dragging = false;

        public CardEventHandler(EventHandler<MouseEvent> onDraggedEventHandler,
                                EventHandler<MouseEvent> onClickedEventHandler,
                                GridPane card, ScrollPane targetArea)
        {
            this.onDraggedEventHandler = onDraggedEventHandler;
            this.onClickedEventHandler = onClickedEventHandler;
            this.card = card;
            this.targetArea = targetArea;

            this.targetArea.addEventHandler(MouseEvent.MOUSE_ENTERED,
                    event -> { libraryViewController.moveCardIntoLibrary(card); });
        }

        @Override
        public void handle(MouseEvent event)
        {
            if (event.getEventType() == MouseEvent.MOUSE_PRESSED)
            {
                dragging = false;
                dragContext.anchorX = event.getX();
                dragContext.anchorY = event.getY();
            }
            else if (event.getEventType() == MouseEvent.DRAG_DETECTED)
            {
                dragging = true;
            }
            else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED)
            {
                onDraggedEventHandler.handle(event);
                card.setTranslateX(event.getX() + card.getTranslateX() - dragContext.anchorX);
                card.setTranslateY(event.getY() + card.getTranslateY() - dragContext.anchorY);
            }
            else if (event.getEventType() == MouseEvent.MOUSE_CLICKED)
            {
                if (!dragging)
                {
                    onClickedEventHandler.handle(event);
                    onCardClicked(event);
                }
            }
            else if (event.getEventType() == MouseEvent.MOUSE_RELEASED)
            {
                card.setTranslateX(0);
                card.setTranslateY(0);
            }
        }
    }
}
