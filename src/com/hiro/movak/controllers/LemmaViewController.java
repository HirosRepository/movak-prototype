package com.hiro.movak.controllers;

import com.hiro.movak.model.entities.Lemma;
import com.hiro.movak.model.services.WordService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

import java.io.IOException;
import java.util.List;

/**
 * @author hiro
 */

public class LemmaViewController
{
    /* Private member variables ***************************************************************************************/

    private ReaderViewController readerViewController = null;

    @FXML
    private TextField lemma_textfield;

    @FXML
    private VBox candidates;

    @FXML
    private Button submit_button;

    // Services
    private WordService wordService = new WordService();

    // Other information
    private String wordForm;

    /* Public methods *************************************************************************************************/

    /**
     *
     * @param readerViewController
     */

    public void setReaderViewController(ReaderViewController readerViewController)
    {
        this.readerViewController = readerViewController;
    }

    /**
     *
     * @param wordForm
     */

    public void setWordForm(String wordForm)
    {
        this.wordForm = wordForm;
        List<Pair<String, String>> lemmaCandidates = wordService.getLemmaCandidates(wordForm, true);

        lemma_textfield.setText(wordForm);
        if (!lemmaCandidates.isEmpty())
        {
            for (Pair<String, String> candidate : lemmaCandidates)
            {
                loadSuggestion(candidate);
            }
        }
    }

    /**
     *
     */

    public void closeView(Lemma selectedLemma)
    {
        readerViewController.clearSideBar();
        readerViewController.loadPairView(selectedLemma);
    }

    /* Event callbacks ************************************************************************************************/

    /**
     *
     * @param mouseEvent
     */

    public void onSubmitButtonClicked(MouseEvent mouseEvent)
    {
        submitLemma();
    }

    /* Private methods ************************************************************************************************/

    /**
     *
     * @param candidate
     */

    private void loadSuggestion(Pair<String, String> candidate)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/lemma_suggestion_card.fxml"));
            GridPane lemmaSuggestionCard = loader.load();
            LemmaSuggestionCardController controller = loader.getController();
            controller.setLemmaViewController(this);

            controller.setSuggestion(candidate.getKey(), candidate.getValue());

            candidates.getChildren().add(lemmaSuggestionCard);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */

    private void submitLemma()
    {
        String lemmaString = lemma_textfield.getText();
        Lemma lemma = wordService.createLemma(lemmaString, true);
        closeView(lemma);
    }
}
