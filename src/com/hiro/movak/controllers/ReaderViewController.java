package com.hiro.movak.controllers;

import com.hiro.movak.model.entities.Lemma;
import com.hiro.movak.model.entities.UserTextDetails;
import com.hiro.movak.model.utils.Token;
import com.hiro.movak.model.services.TextService;
import com.hiro.movak.model.utils.TextFlowFormatter;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The controller for the reader view.
 * @author hiro
 */

public class ReaderViewController
{
    /* Private member variables ***************************************************************************************/

    // Static UI components
    @FXML
    private GridPane root;

    @FXML
    private Pane text_area;

    @FXML
    private VBox side_bar;

    @FXML
    private ProgressBar progress_bar;

    // Dynamic UI components
    private GridPane token_view;
    private GridPane lemma_view;
    private GridPane pair_view;
    private TextFlow mainText = new TextFlow();

    // Services
    private TextService textService = new TextService();

    // Other information
    private UserTextDetails userTextDetails = null;
    private List<Token> tokens = new ArrayList<>();
    private List<Integer> idList;
    private int textId;
    private int location = 0;
    private int length = 300;   // fixme: dynamically set this
    private int maxLength;
    private float progress = 0.0f;

    /* Public methods *************************************************************************************************/

    /**
     * Initializes root, text_area and mainText components.
     */

    @FXML
    public void initialize()
    {
        // Initialize root component
        initRoot();

        // Initialize text_area component
        initTextArea();

        // Initialize main_text component
        initMainText();
    }

    /**
     * Sets textId.
     * @param textId id of the text to be read
     */

    public void setTextId(int textId)
    {
        this.textId = textId;
        userTextDetails = textService.getUserTextDetails(textId);
        location = userTextDetails.getLastLocation();
        idList = textService.getIdList(textId);
        maxLength = idList.size();
        progress_bar.setProgress(userTextDetails.getProgress());
        loadText();
        mainText.needsLayoutProperty().addListener((obs,d,d1)->setBackgroundColors());
    }

    /**
     * Remove all components in the side bar. Called from other controllers.
     */

    public void clearSideBar()
    {
        side_bar.getChildren().clear();
    }

    public void loadPairView(Lemma selectedLemma)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/layouts/pair_view.fxml"));
            pair_view = loader.load();
            PairViewController pairViewController = loader.getController();
            pairViewController.setSourceInfo(textId, selectedLemma);
            pairViewController.setReaderViewController(this);

            side_bar.getChildren().clear();
            side_bar.getChildren().add(pair_view);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void reloadText()
    {
        loadText();
        root.requestFocus();
    }

    /* Event callbacks ************************************************************************************************/

    /**
     * Called when a token is clicked. Called by a Token object.
     * @param token reference to the caller
     */

    public void onTokenClicked(Token token)
    {
        try
        {
            FXMLLoader loader;

            // When lemma is not recognized
            if (token.getType() == Token.Type.UNKNOWN)
            {
                loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../resources/layouts/lemma_view.fxml"));
                lemma_view = loader.load();
                LemmaViewController lemmaViewController = loader.getController();
                lemmaViewController.setReaderViewController(this);

                lemmaViewController.setWordForm(token.getName());

                side_bar.getChildren().clear();
                side_bar.getChildren().add(lemma_view);
            }
            else
            {
                // todo: Choose pair
                System.out.println("Display pair");
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /* Private methods ************************************************************************************************/

    /**
     * Initializes root by setting keypress listener and by requesting focus to enable it.
     */

    private void initRoot()
    {
        root.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent keyEvent)
            {
                if (keyEvent.getCode() == KeyCode.RIGHT)
                    getNextPage();
                if (keyEvent.getCode() == KeyCode.LEFT)
                    getLastPage();
            }
        });

        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                root.requestFocus();
            }
        });
    }

    /**
     * Initializes mainText by setting the style and setting the change listener.
     */

    private void initMainText()
    {
        // Set style, question: can this be set statically?
        mainText.setPadding(new Insets(30));
        mainText.setStyle("-fx-font-family: Arial Black; -fx-font-size: 20px; -fx-line-spacing: 5px;");
        mainText.setTextAlignment(TextAlignment.JUSTIFY);

        // Update the text if content changes
        mainText.getChildren().addListener(new ListChangeListener<Node>()
        {
            @Override
            public void onChanged(Change<? extends Node> change) { }
        });
    }

    /**
     * Initializes the text_area by setting the change listener to update the size of the mainText.
     */

    private void initTextArea()
    {
        // Update the width and height of mainText based on text_area resize
        text_area.widthProperty().addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1)
                {
                    mainText.setPrefWidth(t1.doubleValue());
                }
            });

        text_area.getChildren().add(mainText);
    }

    /**
     * Moves to the next page by updating the mainText content. Also updates the UserTextDetails.
     */

    private void getNextPage()
    {
        if (location + length < maxLength)
            location += length;
        else
            System.out.println("handle completion here");   // fixme: handle completion

        progress = (float) location / (float) maxLength;
        updateUserTextDetails();
        loadText();
    }

    /**
     * Moves to the last page by updating the mainText content. Also updates the UserTextDetails.
     */

    private void getLastPage()
    {
        location = Math.max(0, location - length);

        progress = (float) location / (float) maxLength;
        updateUserTextDetails();
        loadText();
    }

    /**
     * Updates the progress and the location values of the UserTextDetails object.
     */

    private void updateUserTextDetails()
    {
        progress_bar.setProgress(progress);
        userTextDetails.setProgress(progress);
        userTextDetails.setLastLocation(location);
        textService.updateUserTextDetails(userTextDetails);
    }

    /**
     * Highlights the background of the word tokens based on the type.
     */

    private void setBackgroundColors()
    {
        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                TextFlowFormatter textFlowFormatter = new TextFlowFormatter();
                String css = textFlowFormatter.getCss(mainText, tokens);
                mainText.setStyle( css + "; -fx-background-radius: 2;");
            }
        });
    }

    /**
     * Loads new content to the mainText area. Called whenever the page is turned.
     */

    private void loadText()
    {
        List<Token> wordList = textService.getTextContent(idList, location, Math.min(length, maxLength - location));

        mainText.getChildren().clear();
        for (Token word : wordList)
        {
            word.setReaderViewController(this);
            word.setFont(new Font("Ariana Black", 25));
            tokens.add(word);
            mainText.getChildren().add(word);
        }
    }
}
