package com.hiro.movak.main;

import com.hiro.movak.model.database.Database;
import com.hiro.movak.model.entities.Text;
import com.hiro.movak.model.services.LanguageService;
import com.hiro.movak.model.services.UserService;
import com.hiro.movak.model.services.WordService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.hibernate.Transaction;

import java.io.IOException;
import java.util.List;

public class Main extends Application
{
    private Stage primaryStage;
    private GridPane rootLayout;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Movak");

        Database db = Database.getInstance();

        //LanguageService languageService = new LanguageService();
        //languageService.loadConstructions("/Users/hiroyoshiyamasaki/IntellijProjects/Movak-Prototype/src/com/hiro/movak/resources/construction_rules.json");
        UserService userService = new UserService();
        userService.setDefaultUser();

        initRootLayout();
    }

    public void initRootLayout()
    {
        try
        {
            rootLayout = (GridPane) FXMLLoader.load(getClass().getResource("../resources/layouts/main_window.fxml"));
            rootLayout.getStylesheets().add(getClass().getResource("../resources/style.css").toExternalForm());

            primaryStage.setScene(new Scene(rootLayout, 1200, 700));
            primaryStage.show();
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
